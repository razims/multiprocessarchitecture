﻿using System;

namespace MultiProcessArchitecture.Shared
{
    public sealed class Protocol
    {
        public string Message { get; }

        public Protocol(string message)
        {
            Message = message;
        }
    }
}