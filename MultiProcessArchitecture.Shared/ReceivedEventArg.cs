﻿using System;

namespace MultiProcessArchitecture.Shared
{
    internal class ReceivedEventArg : EventArgs
    {
        public string Data { get;  }

        public ReceivedEventArg(string paramData)
        {
            this.Data = paramData;
        }
    }
}