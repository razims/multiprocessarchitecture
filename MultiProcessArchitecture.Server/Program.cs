﻿using System;
using System.Collections.Generic;

namespace MultiProcessArchitecture.Server
{
    class Program
    {
        private static List<ProcessHost> _procesesList = new List<ProcessHost>();
        
        static void Main(string[] args)
        {
            Console.WriteLine("////////////////////////////////////////////");
            Console.WriteLine("\t \t Press Enter to stop ");
            Console.WriteLine("////////////////////////////////////////////");
            Console.WriteLine("\n\n");
      
            LoadApps();
      
            Console.ReadLine();

            UnloadApps();

            Console.WriteLine("\n\n");
            Console.WriteLine("Press Enter to exit");
            Console.ReadLine();
        }
        
        static void LoadApps()
        {
            for (int i = 1; i <= 10; i++)
            {
                var p = new ProcessHost();
        
                p.Start("IPC_Client" + i.ToString());

                //Trace.WriteLine(string.Format("Process {0} is started", .Id));

                _procesesList.Add(p);

            }
        }

        static void UnloadApps()
        {
            foreach (var p in _procesesList)
            {
                p.Dispose();
            }
        }
    }
}