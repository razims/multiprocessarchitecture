﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.Pipes;
using System.Reflection;
using System.Threading;

namespace MultiProcessArchitecture.Server
{
    internal class ProcessHost : IDisposable
    {
        public const int BufferSize = 1024 * 2;

        private string pipeId;
        private Process childProcess;
        private NamedPipeServerStream pipeServerStream;
        private bool isDisposing;
        private Thread pipeMessagingThread;

        /// <summary>
        /// Constructor
        /// </summary>
        public ProcessHost()
        {
        }

        /// <summary>
        /// Starts the IPC server and run the child process
        /// </summary>
        /// <param name="paramUid">Unique ID of the named pipe</param>
        /// <returns></returns>
        public bool Start(string paramUid)
        {
            pipeId = paramUid;

            pipeMessagingThread = new Thread(new ThreadStart(StartIpcServer));
            pipeMessagingThread.Name = this.GetType().Name + ".PipeMessagingThread";
            pipeMessagingThread.IsBackground = true;
            pipeMessagingThread.Start();

            var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

#if DEBUG
            var processInfo = new ProcessStartInfo()
            {
                FileName = "dotnet",
                Arguments = $"{path}\\MultiProcessArchitecture.Client.dll {this.pipeId}"
            };
#else
            var processInfo = new ProcessStartInfo()
            {
                FileName = $"{path}\\MultiProcessArchitecture.Client.exe",
                Arguments = $"{this.pipeId}"
            };
            #endif


            childProcess = Process.Start(processInfo);

            return true;
        }

        /// <summary>
        ///  Send message to the child process
        /// </summary>
        /// <param name="paramData"></param>
        /// <returns></returns>
        public bool Send(string paramData)
        {
            return true;
        }

        /// <summary>
        /// Start the IPC server listener and wait for
        /// incomming messages from the appropriate child process
        /// </summary>
        void StartIpcServer()
        {
            if (pipeServerStream == null)
            {
                pipeServerStream = new NamedPipeServerStream(pipeId,
                    PipeDirection.InOut,
                    1,
                    PipeTransmissionMode.Byte,
                    PipeOptions.Asynchronous,
                    BufferSize,
                    BufferSize);
            }

            // Wait for a client to connect
            Console.WriteLine("{0}:Waiting for child process connection...", pipeId);
            try
            {
                //Wait for connection from the child process
                pipeServerStream.WaitForConnection();
                Console.WriteLine("Child process {0} is connected.", pipeId);
            }
            catch (ObjectDisposedException exDisposed)
            {
                Console.WriteLine("StartIPCServer for process {0} error: {1}", this.pipeId, exDisposed.Message);
            }
            catch (IOException exIo)
            {
                Console.WriteLine("StartIPCServer for process {0} error: {1}", this.pipeId, exIo.Message);
            }

            var retRead = true;

            while (retRead && !isDisposing)
            {
                retRead = StartAsyncReceive();
                Thread.Sleep(30);
            }
        }

        /// <summary>
        /// Read line of text from the connected client
        /// </summary>
        /// <returns>return false on pipe communication exception</returns>
        bool StartAsyncReceive()
        {
            var sr = new StreamReader(pipeServerStream);
            try
            {
                var str = sr.ReadLine();

                if (string.IsNullOrEmpty(str))
                {
                    // The client is down
                    return false;
                }

                Console.WriteLine("{0}: Received: {1}. (Thread {2})", pipeId, str, Thread.CurrentThread.ManagedThreadId);
            }
            // Catch the IOException that is raised if the pipe is broken
            // or disconnected.cd
            catch (Exception e)
            {
                Console.WriteLine("AsyncReceive ERROR: {0}", e.Message);
                // return false;
                throw;
            }

            return true;
        }

        /// <summary>
        /// Dispose the client process
        /// </summary>
        void DisposeClientProcess()
        {
            try
            {
                isDisposing = true;

                try
                {
                    //I will fails if the process doesn't exist
                    childProcess.Kill();
                }
                catch
                {
                }

                pipeServerStream.Dispose(); //This will stop any pipe activity

                Console.WriteLine("Process {0} is Closed", pipeId);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Process {0} is Close error: {1}", this.pipeId, ex.Message);
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            DisposeClientProcess();
        }

        #endregion
    }
}