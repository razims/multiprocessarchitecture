﻿using System;

namespace MultiProcessArchitecture.Client
{
    class Program
    {
        static void Main(string[] args)
        {
            var ctx = new AppContext(args[0]);
            Console.ReadLine();
        }
    }
}