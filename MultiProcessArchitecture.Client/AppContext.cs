﻿using System;
using System.IO;
using System.IO.Pipes;
using System.Net.Mime;
using MultiProcessArchitecture.Shared;

namespace MultiProcessArchitecture.Client
{
    internal class AppContext
    {
        private string id;
        private NamedPipeClientStream pipeClientStream;
        private readonly System.Timers.Timer keepAliveTimer = new System.Timers.Timer(2000);

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="paramId"></param>
        public AppContext(string paramId)
        {
            // Handle the ApplicationExit event to know when the application is exiting.
            id = paramId;

            StartIPC();

            keepAliveTimer.Elapsed += KeepAliveTimer_Elapsed;
            keepAliveTimer.Interval = 2000;
            keepAliveTimer.Start();
        }

        /// <summary>
        /// Sending message to the parent application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void KeepAliveTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                if (pipeClientStream.IsConnected)
                {
                    var sw = new StreamWriter(pipeClientStream);
                    sw.WriteLine("Message from " + id);
                    sw.Flush();
                    System.Diagnostics.Trace.WriteLine("KeepAlive was sent by " + id);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("pipeClient is disonnected for " + id + "Error: " + ex);
                throw;
            }
        }

        /// <summary>
        /// Connectin to the IPC server
        /// </summary>
        private void StartIPC()
        {
            System.Diagnostics.Trace.WriteLine("Starting IPC client for " + id);
            pipeClientStream = new NamedPipeClientStream(".",
                id,
                PipeDirection.InOut,
                PipeOptions.Asynchronous);

            try
            {
                pipeClientStream.Connect(3000);
                System.Diagnostics.Trace.WriteLine("Connected to IPC server. " + id);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("StartIPC Error for " + ex);
                throw;
            }
        }
    }
}